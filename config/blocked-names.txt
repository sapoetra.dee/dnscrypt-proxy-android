
###########################
#        Blocklist        #
###########################

## Author      : quindecim   : https://codeberg.org/quindecim
##                             https://git.nixnet.services/quindecim
##
## License     : GPLv3       : https://codeberg.org/quindecim/dnscrypt-proxy-android/src/branch/master/LICENSE.md
##
##
## DO NOT DELETE THIS FILE !!
##
## This file is intended to be a placeholder, you can use it at your pleasure to filter your content on the web.
## More info at: https://github.com/DNSCrypt/dnscrypt-proxy/wiki/Filters
